
package ssf.pkman;

import java.sql.*;
import java.net.*;
import java.io.*;
import java.util.*;

//Clase principal
class Main {
	private RequestsPool requestPool;
	private String 			 jdbcurl;
	private int					 poolsize;
	private int					 puerto;
	private Thread   		 thrListener,thrPKServer,thrDBUpdater;
	private Runnable 		 runListener,runPKServer,runDBUpdater;

	public static void main(String arg[]){
		new Main(arg);
	}

	Main(String arg[]){
		int    	opt[]; //Tiene en cuenta el parámetro opcional
		String 	dbtype,
						jdbcurl,
						login,
						passwd;
		int			port,offset=0;


		//Debe haber 5 argumentos
		opt = new int[2];
		if( arg.length < 5 ){
			Instrucciones();
		}else{
			for(int na=0;na<arg.length-5;na++){
				if( arg[na].equalsIgnoreCase("-init") ){
					opt[0]=1;
					offset++;
				}else if(arg[na].equalsIgnoreCase("-debug") ){
					opt[1]=1;
					offset++;
				}else{
					System.out.println("No reconozco "+arg[na]);
					Instrucciones();
				}
			}
		}

		dbtype =arg[offset+0];
		jdbcurl=arg[offset+1];
		login  =arg[offset+2];
		passwd =arg[offset+3];
		port   =Integer.parseInt(arg[offset+4]);

		requestPool=new RequestsPool();

		try {
			runListener = new pkListener(requestPool,opt,port );
			runPKServer = new pkServer  (requestPool,opt);
			runDBUpdater= new dbUpdater (opt,dbtype,jdbcurl,login,passwd);
		}catch(Exception e){
			e.printStackTrace();
			System.exit(-1);
		}

		//Se arranca ordenadamente las 3 hebras
		//Monitorizar los thread
		for(;;){
			try {
				if( thrDBUpdater==null || !thrDBUpdater.isAlive() ){
					System.out.println("Main: Thread DBUpdater, iniciando");
					thrDBUpdater = new Thread    ( runDBUpdater );
					thrDBUpdater.start();
				}
				if( thrPKServer==null || !thrPKServer.isAlive() ){
					System.out.println("Main: Thread PKServer,  iniciando");
					thrPKServer = new Thread    ( runPKServer );
					thrPKServer.start();
				}
				if( thrListener==null|| !thrListener.isAlive() ){
					System.out.println("Main: Thread pkListener, iniciando");
					thrListener= new Thread    (runListener);
					thrListener.start();
				}
				Thread.sleep(5000); //Cada 5 segundos
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	private void Instrucciones(){
		System.out.println("\nUso: java -jar ospkman.jar [-init] [-debug] driver url log passwd port\n");
		System.out.println("\t[-init] : Recalcula los PK de cada tabla");
		System.out.println("\t[-debug]: Presenta mensajes de depuracion");
		System.out.println("\tdriver  : [ oracle | sqlserver | sybase | informix ]");
		System.out.println("\turl     : (oracle)    host:port:sid");
		System.out.println("\t          (sqlserver) host:port");
		System.out.println("\t          (sybase)    host:port");
		System.out.println("\t          (informix)  database@host:port");
		System.out.println("\tlog     : Usuario de BBDD");
		System.out.println("\tpasswd	: Password del usuario de BBDD");
		System.out.println("\tport    : Puerto de escucha de este servicio.\n");
		System.exit(-1);
	}
}
			