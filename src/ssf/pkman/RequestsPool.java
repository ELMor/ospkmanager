
package ssf.pkman;

import java.sql.*;
import java.util.*;
import java.net.*;

class RequestsPool {
	private Vector htr;

	RequestsPool(){
		htr=new Vector();
	}

	public synchronized void insert(Integer descriptor, Integer rng, Socket sc){
		Request req=new Request(descriptor.intValue(),sc,rng.intValue());
		htr.add(req);
		notifyAll();
	}

	public synchronized Request getRequest()
		throws InterruptedException {
		//Obtener un request o varios sobre el mismo descriptor.
		for(;;)
			if( htr.size()>0 ){
				Request rq=(Request)htr.elementAt(0);
				htr.removeElementAt(0);
				return rq;
			}else{
				wait();
			}
	}

}