
package ssf.pkman;

import java.sql.*;
import java.util.*;

//Clase que se conecta a la BBDD y la va refrescando
class dbUpdater extends Thread{

	Connection conn=null; //Conexi�n de BBDD
	PreparedStatement stmt1=null,stmt2=null;
	ResultSet rs=null;
	int offset;
	int debug;

	dbUpdater(int opt[], String dbtype, String surl, String login, String pass)
		throws Exception {

		Properties props = new Properties();
		props.put("user"    , login);
		props.put("password", pass);
		if( dbtype.equalsIgnoreCase("oracle") ){
			//Orcacle 9i
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			surl="jdbc:oracle:thin:@"+surl;
		}else if( dbtype.equalsIgnoreCase("sqlserver") ){
			//Dos opciones: el controlador de Weblogic o el de Microsoft
			//Weblogic:
			/*
			surl="jdbc:weblogic:mssqlserver4:"+surl;
			Class.forName("weblogic.jdbc.mssqlserver4.Driver").newInstance();
			*/
			//Microsoft
			surl="jdbc:microsoft:sqlserver://"+surl;
			Class.forName("com.microsoft.jdbc.sqlserver.SQLServerDriver");
		}else if( dbtype.equalsIgnoreCase("sybase") ){
			//Sybase ASE, ASA
			surl="jdbc:sybase:Tds:"+surl;
			props.put("USE_METADATA","false");
			Class.forName("com.sybase.jdbc2.jdbc.SybDriver").newInstance();
		}else if( dbtype.equalsIgnoreCase("informix") ){
			//Informix Dynamic Server
			surl="jdbc:weblogic:informix4:"+surl;
			Class.forName("weblogic.jdbc.informix4.Driver").newInstance();
		}else{
			System.out.println("BD:"+dbtype+" No encontrada");
			throw new Exception(dbtype+" Not found");
		}

		conn=DriverManager.getConnection(surl,props);

		//Inicializamos las sentencias SQL
		String sql1,sql2;
		sql1="select contador from descriptores where codigo_descriptor=?";
		sql2="update descriptores set contador=? where codigo_descriptor=?";
		stmt1=conn.prepareStatement(sql1);
		stmt2=conn.prepareStatement(sql2);

		offset=opt[0];
		debug=opt[1];
	}

	private boolean chkMetaData(DatabaseMetaData dmd, Statement stupd,
	                            String tabla, long descr)
		throws SQLException {
		ResultSet rsmd;
		rsmd=dmd.getPrimaryKeys(null,null,tabla);
		if( rsmd.next() ){
			String sql2="update descriptores set contador=(select 1+max("+
			            rsmd.getString(4)+") from "+tabla+")"+" where "+
			            "codigo_descriptor="+descr;
			System.out.println(sql2);
			stupd.executeUpdate(sql2);
			return true;
		}else{
			return false;
		}
	}


	void calcContadores() throws Exception{
		DatabaseMetaData dmd;
		ResultSet rsmd,rsdes;
		String sql;
		Statement st,stupd;

		dmd=conn.getMetaData();

		st   =conn.createStatement();
		stupd=conn.createStatement();

		sql="select descriptor,codigo_descriptor from descriptores "+
		    "where tdesc_pk=1";
		rsdes=st.executeQuery(sql);

		while( rsdes.next() ){
			//Probamos primero con lo que se retorna desde descriptores
			String tabla=rsdes.getString(1);
			long   descr=rsdes.getLong(2);
			System.out.println("Tabla "+tabla+":");
			if( !chkMetaData(dmd,stupd,tabla,descr) )
				if( !chkMetaData(dmd,stupd,tabla.toLowerCase(),descr) )
					if( !chkMetaData(dmd,stupd,tabla.toUpperCase(),descr) )
						System.out.println("No tiene PK!");
		}
		rsdes.close();
		System.out.println("Fin de actualizacion. Sirviendo a clientes.");
	}

	void initContadores() throws Exception{
		long cod_des,sigval,rng;
		String sql="select codigo_descriptor,contador,mascara from descriptores "+
							 "where tdesc_pk=1";
		Statement st=conn.createStatement();
		rs=st.executeQuery(sql);
		while( rs.next() ){
			cod_des=rs.getLong(1);
			try {
				rng=rs.getLong(3);
			}catch(Exception e){
				rng=1;
			}
			if( rs.wasNull() )
				rng=1;
			sigval=rs.getLong(2);
			if( rs.wasNull() )
				sigval=1;
			new Contador( cod_des, sigval,rng, rs.wasNull() );
		}
	}

	public void run(){
		try {
			if(offset==1)
				calcContadores();
			initContadores();
		}catch(Exception e){
			e.printStackTrace();
			System.exit(0);
		}
		for(;;){
			try {
				Thread.sleep(2000); // Cada 2 Segundos
				Hashtable ht=Contador.getCTU();
				Enumeration cod_des=ht.keys(),
										conta  =ht.elements();
				while( cod_des.hasMoreElements() ){
					Long     ds=(Long)    cod_des.nextElement();
					Contador ct=(Contador)  conta.nextElement();
					long nval=ct.getUltValVal(0)+5*ct.getRango();
					stmt2.setLong(1,nval);
					stmt2.setLong(2,ds.longValue());
					stmt2.addBatch();
					ct.setUltimoPKSincronizado(nval);
					ht.remove( ds );
				}
				stmt2.executeBatch();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}