package ssf.pkman;

import java.sql.*;
import java.net.*;
import java.util.*;
import java.io.*;

//Esta clase sirve las peticiones de los clientes
class pkServer extends Thread {
	RequestsPool					requestPool;
	String								sql1,sql2;
	int										offset,debug;

	pkServer(RequestsPool vrp, int opt[])
		throws Exception {
		requestPool=vrp;
		offset=opt[0];
		debug=opt[1];
	}

	String normNumber(long l){
		StringBuffer ret=new StringBuffer("0000000000");
		String s=new Long(l).toString();
		ret.setLength(10-s.length());
		ret.append(s);
		return ret.toString();
	}

	public void run(){
		Request  rq=null;
		Socket   sc=null;
		long		 rn=0;
		int			 de=0;
		for(;;){ //Bucle infinito
			try {
				rq=requestPool.getRequest();
				sc=rq.getSocket();
				rn=rq.getRango();
				de=rq.getDescriptor();

				Contador ct=Contador.getContador( de );
				long endPK =ct.getUltValVal( rn );
				long iniPK =endPK-rn+1;

				sc.getOutputStream().write((normNumber(iniPK)+normNumber(endPK)).getBytes() );
				sc.close();
			}catch(Exception e){
				try {
					if(sc!=null)
						sc.getOutputStream().write("00000000-100000000-1".getBytes() );
				}catch(Exception e2){
				}
				try{
					if(sc!=null)
						sc.close();
				}catch(Exception e3){
				}
			}
		}
	}
}
