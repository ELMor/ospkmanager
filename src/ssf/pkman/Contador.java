

package ssf.pkman;

import java.util.*;

//Esta clase es un contador (un codigo_descriptor de la tabla descriptores)
class Contador {
	//Lista con los contadores que necesitan ser refrescados
	static Hashtable ctu=new Hashtable();
	//Lista de todos los contadores.
	static Hashtable ctd=new Hashtable();
	long	 codigo_descriptor;
	long	 siguientePK;
	long	 rango;
	long	 ultimoPKSincronizado;

	static Contador getContador(long i){
		return (Contador)(ctd.get( new Long(i) ));
	}

	static Hashtable getCTU(){
		return ctu;
	}

	Contador(long cod_des, long sigPK, long rng, boolean fromDB){
		codigo_descriptor=cod_des;
		siguientePK=sigPK;
		ultimoPKSincronizado=sigPK;
		rango=rng;
		ctd.put( new Long(codigo_descriptor), this);
	}

	synchronized long getUltValVal(long rango){ //Devuelve el ultimo valor v�lido
		siguientePK+=rango;
		if(siguientePK>ultimoPKSincronizado)
			ctu.put( new Long(codigo_descriptor), this);
		return siguientePK-1;
	}

	synchronized void setUltimoPKSincronizado(long upk){
		ultimoPKSincronizado=upk;
	}

	long getRango(){
		return rango;
	}

}